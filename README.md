Project is under development. 

The project is a solution for a Virtual Animal Shelter. Key functionalities:

- admin panel from where advertisements can be accepted to go on the main page
- adding advertisements about animals to take
- uploading photos of animals and some info about them
- sorting based on categories (e.g. cats, dogs) and filters (e.g. oldest in the shelter)

Starting project: 
Work is done on the develop branch, so:

1. Clone the project over https
2. Checkout the develop branch
3. npm i
4. npm start